package com.platfarm.example.mojitoksdksampleapp

import android.util.Log
import androidx.multidex.MultiDexApplication
import com.mojitok.mojitokcore.Mojitok
import com.mojitok.mojitokcore.schema.pay.MJTInAppProductSkuHolder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        // 2-1. Initialize Mojitok module
        // 2-1. Mojitok 모듈 초기화
        val mojitokApplicationId = "{YOUR-MOJITOK-APPLICATION-ID}"
        Mojitok.setup(
            mojitokApplicationId, // Application ID
            applicationContext
        )

        // 2-2. Register the information of in-app products registered on Google Play in the Mojitok module
        // 2-2. Mojitok 모듈에 Google Play 에 등록한 인앱상품 정보를 등록
        Mojitok.registerInAppProducts(MJTInAppProductSkuHolder(stickerPackSku="stickerpack"))

    }
}