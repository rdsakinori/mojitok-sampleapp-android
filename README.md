# MojitokSDK-Android-SampleApp


## 0. Quick Start with SampleApp APK

Download and install [MojitokSDKSampleApp.apk](https://drive.google.com/file/d/1AVhue-0B25DoZGFR1yFaYhmepELGSEWo/view?usp=sharing) on your Android phone and you can test it right away.

## 1. Quick Start with SampleApp Code

#### Step1. Clone the SampleApp Code

```
git clone https://gitlab.com/mojitok/sdk/android/mojitok-sampleapp-android.git
```

#### Step2. Add provided SDK file to the app/libs/ folder.

#### Step3. Initialize parameters

```
Mojitok.setup("ApplicationID", "userId", ..)
Mojitok.connect("ApplicationToken", ..)
```

Enter the issued [ApplicationId](https://gitlab.com/mojitok/sdk/android/mojitok-sampleapp-android/-/blob/master/app/src/main/java/com/platfarm/example/mojitoksdksampleapp/App.kt#L23) and [ApplicationToken](https://gitlab.com/mojitok/sdk/android/mojitok-sampleapp-android/-/blob/master/app/src/main/java/com/platfarm/example/mojitoksdksampleapp/App.kt#L36).
The [userId](https://gitlab.com/mojitok/sdk/android/mojitok-sampleapp-android/-/blob/master/app/src/main/java/com/platfarm/example/mojitoksdksampleapp/App.kt#L24)(shorten for userIdentifierOfYourApp) field is an identifier that identifies individual end users of your app, and you can enter an arbitrary value when testing.

That's it!
